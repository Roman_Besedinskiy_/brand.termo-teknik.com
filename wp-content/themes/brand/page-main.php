<?php
/**
 * Template Name: Main Page
 */


	get_header(); 
?>

	<div id="main">

		<?php //echo $page[0]->post_content; ?>

		    <?php get_template_part( 'partials/top', 'page' ); ?>
			<?php echo do_shortcode('[widget id="ctup-ads-pointswidget-2"]'); ?>	 
			 <?php //echo do_shortcode('[crp_portfolio id=1]'); ?>
			<?php get_template_part( 'partials/presentation', 'page' ); ?>
			<?php echo do_shortcode('[widget id="ctup-ads-aboutwidget-3"]' );?>
			<?php get_template_part( 'partials/how-work', 'page' ); ?>
			<?php echo do_shortcode('[widget id="ctup-ads-workswidget-2"]');?>
			<?php get_template_part( 'partials/what-we-do', 'page' ); ?>
			<?php get_template_part( 'partials/our-clients', 'page' ); ?>
			<?php //do_action( 'woothemes_our_team' ); ?>
			<div class="text-center"> <h2>Наша команда</h2> </div>
			<?php echo do_shortcode('[widget id="ctup-ads-statisticswidget-2"]');?>
			<?php echo do_shortcode('[widget id="ctup-ads-resultswidget-2"]');?>

			<?php //get_template_part( 'partials/results', 'page' ); ?>
			<?php echo do_shortcode('[testimonials]');?>
			<?php //get_template_part( 'partials/testimotials', 'page' ); ?>
			<section class="contact-us section">
			 <div class="text-center">	  
			 	<h2>Напишите нам</h2>
				<?php  echo do_shortcode('[contact-form-7 id="30" title="Напишите нам"]');
				//get_template_part( 'partials/contact-us', 'page' ); ?>
			</div>
			</section>
			<?php //get_template_part( 'partials/news', 'page' ); ?>
			


	</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>