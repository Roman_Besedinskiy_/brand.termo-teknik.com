<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		<footer class="footer">
			<div class="category text-center">
				<?php echo do_shortcode('[widget id="nav_menu-2"]') ?>
<!-- 					<ul> 
				 	 	<li> <a>Корпоративные брошюры</a> <span class="line"></span></li>
				 	 	<li> <a>Каталоги продукци</a> <span class="line"></span></li>
				 	    <li> <a>Периодические издания: журналы / газеты</a> <span class="line"></span></li>
				 	    <li> <a>Корпоративные книги</a> <span class="line"></span></li> 	
				 	    <li> <a>Юбилейные издания</a> <span class="line"></span></li> 	
				 	    <li> <a>Годовые отчеты</a> <span class="line"></span></li> 	
				 	    <li> <a>Маркетинг-киты</a> <span class="line"></span></li> 
				 	    <li> <a>Издания по итогам мероприятия</a> <span class="line"></span></li> 		
				 	</ul> -->
				 	<span class="lg"><img src="<?php echo get_header_image(); ?>" /> </span>
			</div>	
			<div class="mn-top-bar">
			 <div class="container">
			 	<div id="social" class="pull-left">
					<a class="instagram"></a>
					<a class="facebook"></a>
					<a class="vk"></a>
					<!-- <span class="cp"> © - РПК "БРЭНД" - Дунаев Александр Александрович - 2016 Все права защищены. </span> -->
				</div>
				<!-- <span class="lg"><img src="<?php echo get_header_image(); ?>" /> </span> -->
				<div class="pull-right ct mn-top-contact">
					<ul>
					    <li><i class="fa fa-envelope"></i><a href="mailto:zakaz@1spress.ru">zakaz@1spress.ru</a></li>
						<li><a href="#" data-toggle="modal" data-target="#myModal"> заказать обратный звонок</a></li>
						<li style="font-size: 18px;font-weight: bold;"><i class="fa fa-phone"></i> + 7(495) 507-78-02</li>	
				    </ul>
			     </div>
			</div>
			</div>
		    <p class="text-center">
		    <span style="padding-top: 10px;"> © - РПК "БРЭНД" - Дунаев Александр Александрович - 2016 Все права защищены. </span> 
		    <a href="http://www.liveinternet.ru/click" target="_blank"><img src="//counter.yadro.ru/hit?t26.1;r;s1366*768*24;uhttp%3A//www.1spress.ru/;0.40947528258498966" alt="" title="LiveInternet: показано число посетителей за сегодня" border="0" width="88" height="15"></a>
		    </p>
		</footer>
	</div><!-- #page -->

	<script src="<?php echo get_template_directory_uri ()?>/lightbox/lightbox.min.js"></script>
	<?php wp_footer(); ?>
</body>
</html>