<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<style>
.video-js{
    margin: 0 auto;
    max-height: 612px;
}
</style>
	<div id="main">
	<div class="top-slider">
		 <ul class="rslides top-slider">
                <li>
                  <div class="top-img">
		 		 <video id="myVideo" controls="false" muted="false" width="100%" height="auto" poster="/wp-content/themes/brand/img/Tulips.jpg" autoplay="autoplay" title="depositphotos_50714611-Summer-flowers-blooming" loop="loop" onended="var v=this;setTimeout(function(){v.play()},300)">
						<source src="/wp-content/themes/brand/video/big_buck_bunny.mp4" type="video/mp4" />
						<source src="/wp-content/themes/brand/video/big_buck_bunny.webm" type="video/webm" />
						<source src="/wp-content/themes/brand/video/big_buck_bunny.ogv" type="video/ogg" /> 
				</video>
		 	      </div>
		 	    </li>
                <li>
                  <div class="top-img">
		 		     <img src="/wp-content/themes/brand/img/Desert.jpg" />
		 	      </div>
		 	    </li>
                <li>
                  <div class="top-img">
		 		     <img src="/wp-content/themes/brand/img/Desert.jpg" />
		 	      </div>
		 	    </li>
            </ul>

         </div>
 
<!-- 	 <div class="top-slider">
		 <div class="rslides top-slider" style="margin:0 auto">
		 	<div class="top-img">
				<div class="easyhtml5video" style="position:relative;max-width:100%;">
				<video controls="false"  autoplay="autoplay" poster="eh5v.files/html5video/depositphotos_50714611-Summer-flowers-blooming.jpg" style="width:100%" title="depositphotos_50714611-Summer-flowers-blooming" loop="loop">
						<source src="/wp-content/themes/brand/video/big_buck_bunny.mp4" type="video/mp4" />
						<source src="/wp-content/themes/brand/video/big_buck_bunny.webm" type="video/webm" />
						<source src="/wp-content/themes/brand/video/big_buck_bunny.ogv" type="video/ogg" /> 
			</video>
			</div>
			</div>
		 	<div class="top-img">
		 		<img src="/wp-content/themes/brand/img/Desert.jpg" />
		 	</div>
		 	<div class="top-img">
		 		<img src="/wp-content/themes/brand/img/Tulips.jpg" />
		 	</div>
</div>
</div> -->
	<!-- <div class="videoWrapper">
<style type="text/css">.easyhtml5video .eh5v_script{display:none}</style> -->
<!-- <div class="easyhtml5video" style="position:relative;max-width:450px;"><video controls="false"  autoplay="autoplay" poster="eh5v.files/html5video/depositphotos_50714611-Summer-flowers-blooming.jpg" style="width:100%" title="depositphotos_50714611-Summer-flowers-blooming" loop="loop" onended="var v=this;setTimeout(function(){v.play()},300)">
<source src="/wp-content/themes/brand/video/big_buck_bunny.mp4" type="video/mp4" />
<source src="/wp-content/themes/brand/video/big_buck_bunny.webm" type="video/webm" />
<source src="/wp-content/themes/brand/video/big_buck_bunny.ogv" type="video/ogg" /> -->
<!-- <object type="application/x-shockwave-flash" data="eh5v.files/html5video/flashfox.swf" width="450" height="250" style="position:relative;">
<param name="movie" value="eh5v.files/html5video/flashfox.swf" />
<param name="allowFullScreen" value="true" />
<param name="flashVars" value="autoplay=true&amp;controls=true&amp;fullScreenEnabled=true&amp;posterOnEnd=true&amp;loop=true&amp;poster=eh5v.files/html5video/depositphotos_50714611-Summer-flowers-blooming.jpg&amp;src=depositphotos_50714611-Summer-flowers-blooming.m4v" />
 <embed src="eh5v.files/html5video/flashfox.swf" width="450" height="250" style="position:relative;"  flashVars="autoplay=true&amp;controls=true&amp;fullScreenEnabled=true&amp;posterOnEnd=true&amp;loop=true&amp;poster=eh5v.files/html5video/depositphotos_50714611-Summer-flowers-blooming.jpg&amp;src=depositphotos_50714611-Summer-flowers-blooming.m4v"	allowFullScreen="true" wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_en" />
<img alt="depositphotos_50714611-Summer-flowers-blooming" src="eh5v.files/html5video/depositphotos_50714611-Summer-flowers-blooming.jpg" style="position:absolute;left:0;" width="100%" title="Video playback is not supported by your browser" />
</object> -->
<!-- </video> -->
<!-- 	<iframe width="910px" height="512px" src="//www.youtube.com/embed/qUJYqhKZrwA?autoplay=1&showinfo=0&controls=0&loop=1&iv_load_policy=0" frameborder="0"></iframe> -->
<!-- 	</div> -->
<!-- 	<div class="videoWrapper vjs-fullscreen">
						  <video id="top-video" class="video-js" controls="false" autoplay="true" data-setup='{
        "controls": false,
        "loop": "true",
        "autoplay": true, 
        "preload": "true"}'
						  >
						    <source src="http://vjs.zencdn.net/v/oceans.mp4" type="video/mp4">
						    <source src="http://vjs.zencdn.net/v/oceans.webm" type="video/webm">
						    <source src="http://vjs.zencdn.net/v/oceans.ogv" type="video/ogg">
						    <p class="vjs-no-js">
						      To view this video please enable JavaScript, and consider upgrading to a web browser that
						      <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
						    </p>
						  </video>
						  </div> -->
					  
			<?php //get_template_part( 'partials/top', 'page' ); ?>
			<?php get_template_part( 'partials/points', 'page' ); ?>
			<?php //echo do_shortcode('[videojs mp4="http://techslides.com/demos/sample-videos/small.mp4" width="100%" height="auto" autoplay="true" loop="true" controls="false"]'); ?>
			<?php echo do_shortcode('[[huge_it_slider id="1"]'); ?>
			 <?php //do_action( 'woothemes_our_team' ); ?>
			  <?php //echo do_shortcode('[crp_portfolio id=1]'); ?>
			<?php get_template_part( 'partials/presentation', 'page' ); ?>
			<?php get_template_part( 'partials/why-we', 'page' ); ?>
			<?php get_template_part( 'partials/how-work', 'page' ); ?>
			<?php get_template_part( 'partials/what-we-do', 'page' ); ?>
			<?php get_template_part( 'partials/our-clients', 'page' ); ?>
			<?php get_template_part( 'partials/our-team', 'page' ); ?>
			<?php get_template_part( 'partials/professionals', 'page' ); ?>
			<?php get_template_part( 'partials/testimonials', 'page' ); ?>
			<?php get_template_part( 'partials/contact-us', 'page' ); ?>
			<?php //get_template_part( 'partials/news', 'page' ); ?>
			


		<?php //get_template_part( 'partials/content', 'page' ); ?>
		<?php //if ( have_posts() ) : ?>

			<?php /* The loop */ ?>
			<?php //while ( have_posts() ) : the_post(); ?>
				<?php //get_template_part( 'content', get_post_format() ); ?>
			<?php //endwhile; ?>

			<?php //twentythirteen_paging_nav(); ?>

		<?php //else : ?>
			<?php //get_template_part( 'content', 'none' ); ?>
		<?php //endif; ?>

	</div><!-- #main -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
