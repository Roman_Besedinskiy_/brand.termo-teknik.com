<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=PT+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri ()?>/css/bootstrap.min.css" media="screen"/>
  <?php wp_head(); ?>
  <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri ()?>/css/gray.css">

  <script src="<?php echo get_template_directory_uri ()?>/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri ()?>/slider/responsiveslides.js"></script>
  <script src="<?php echo get_template_directory_uri ()?>/js/jquery.ui.totop.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri ()?>/js/jquery.mousewheel-3.0.6.pack.js"></script>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri ()?>/lightbox/lightbox.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri ()?>/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri ()?>/slider/responsiveslides.css" media="screen"/>
  <script type='text/javascript' src='<?php echo get_template_directory_uri ()?>/js/jquery.gray.js'></script>
 <!--    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri ()?>/slider/jquery.bxslider.css" media="screen"/> -->
  <script type='text/javascript' src='<?php echo get_template_directory_uri ()?>/slider/jquery.bxslider.min.js'></script>
  <script type="text/javascript">

  	jQuery(document).ready(function( $ ) {
   
    $().UItoTop({ easingType: 'easeOutQuart' });

    $("a.scrollto").click(function () {
        var elementClick = $(this).attr("href")
        var destination = $(elementClick).offset().top;
        jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 200);
        return false;
    });


      $(".testimonials .rslides").responsiveSlides({
        auto: false,
        pager: true,
        nav: true,
        speed: 300,
        prevText: "", 
  		  nextText: "", 
      });

      $(".news .rslides").responsiveSlides({
        auto: false,
        pager: true,
        nav: false,
        speed: 300,
      });

      $(".top-slider .rslides").responsiveSlides({
        auto: false,
        pager: true,
        nav: true,
        prevText: "", 
  		  nextText: "", 
      });

      var num=1;

      $('#loadPhotos').click(function () {

        $.ajax({
          url: '/wp-content/themes/brand/our-portfolio.php?num='+num,
          //dataType: 'html',
          success: function(data) {
            console.log(num);
            $('.ajax-space').append(data);
            $('.ajax-space').show(200);
            num++;
            if (num>3) $('#loadPhotos').hide();
          }
        });
        
      });

		});
  	</script>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="wrapper">

  <!-- Modal -->
<div id="myModal" class="modal fade call-request" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Заказ обратного звонка</h4>
      </div>
      <div class="modal-body">
        <p class="text-center">Для связи с нами, оставьте, пожалуйста, свои данные</p>
        <?php echo do_shortcode('[contact-form-7 id="52" title="Заказ обратного звонка"]');?>
      </div>
    </div>

  </div>
</div>

	 <header id="header">
	 <div class="mn-top-bar">
	 <div class="container">
	 	<div id="social" class="col-sm-3 col-sm-3 col-xs-3 pull-left">
			<a class="instagram"></a>
			<a class="facebook"></a>
			<a class="vk"></a>
		</div>
		 <div class="col-md-9 col-sm-9 col-xs-9 mn-cn">
			<ul class="mn-top-contact">
			<li><i class="fa fa-envelope"></i><a href="mailto:zakaz@1spress.ru">zakaz@1spress.ru</a></li>
			<li><a href="#" data-toggle="modal" data-target="#myModal"> заказать обратный звонок</a></li>
			<li style="font-size: 18px;font-weight: bold;"><i class="fa fa-phone"></i> + 7(495) 507-78-02</li>

		</ul>
		</div>
	</div>
	</div>
	 <nav role="navigation" class="navbar navbar-default container">
		  <!-- Brand and toggle get grouped for better mobile display -->
		  <div class="navbar-header mobile">
		    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
		      <span class="sr-only">Toggle navigation</span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		    </button>
		    <a href="#" class="navbar-brand"><img src="<?php echo get_header_image(); ?>" /></a>
		  </div>
		
			<div id="navbarCollapse" class="collapse navbar-collapse">
		    	<ul class="nav navbar-nav">
					 	<!-- <a href="/"> <img src="<?php echo get_header_image(); ?>" /> </a> -->
				 	 	<?php

                  			 $menu = wp_get_nav_menu_object ('Header Menu');
                  			 $menu_items = wp_get_nav_menu_items($menu->term_id);
                  			// print_r($menu_items);
                  			 $middle = (int)(count($menu_items)/2);
                  			
                  			 for($i=0; $i<count($menu_items);$i++):

                  				if ($i==$middle):
                		  	  ?>
                				<a href="/" class="navbar-brand desktop"> <img src="<?php echo get_header_image(); ?>" /> </a>
                			  <?php endif;?>
						 	 	<li> 
                  <?php if ($menu_items[$i]->title == 'Клиенты'): ?>
                        <a href="#clients" class="scrollto"> <?php echo $menu_items[$i]->title; ?> </a>
                  <?php else:?>
						 	 		     <a href="<?php echo $menu_items[$i]->url; ?>"> <?php echo $menu_items[$i]->title; ?></a>
                  <?php endif;?> 
						 	 	</li>
				 	    <?php endfor; ?>
				 	    <li> <a href="#" class="uk-flag"> </a> </li>
				 	 </ul>
				</div>
		</nav>

	 </header>