<?php 

		$images = get_field('top_gallery');

		if( $images ): ?>
			    <?php //print_r($images); ?>
		<?php endif; ?>

	
	<div class="top-slider">
		 <ul class="rslides">
		 <?php foreach ($images as $img): ?>
                <li>
                  <div class="top-img">
		 		     <img src="<?php echo $img['url']?>" title="<?php echo $img['title']?>" alt="<?php echo $img['alt']?>" />
		 	      </div>
		 	    </li>
		 	<?php endforeach; ?>
         </ul>

    </div>

 
  <div class="clearfix"></div>