			 <section class="what_we_do">
				 <div class="section">
					 <h2 class="text-center">Что мы делаем?</h2>
					<div class="container category text-center">
						<ul> 
					 	 	<li> <a>Корпоративные брошюры</a> <span class="line"></span></li>
					 	 	<li> <a>Каталоги продукци</a> <span class="line"></span></li>
					 	    <li> <a>Периодические издания: журналы / газеты</a> <span class="line"></span></li>
					 	    <li> <a>Корпоративные книги</a> <span class="line"></span></li> 	
					 	    <li> <a>Юбилейные издания</a> <span class="line"></span></li> 	
					 	    <li> <a>Годовые отчеты</a> <span class="line"></span></li> 	
					 	    <li> <a>Маркетинг-киты</a> <span class="line"></span></li> 
					 	    <li> <a>Издания по итогам мероприятия</a> <span class="line"></span></li> 		
					 	</ul>
					</div>
					 <div class="gallery image-container">
						<a data-lightbox="example-set" href="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/83c2446a0896df0a1f4af01c940ae1d9_M.jpg" title="Vertical - Special Edition! (cedarsphoto)" data-label="Gray">
							<img src="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/83c2446a0896df0a1f4af01c940ae1d9_M.jpg" class="grayscale grayscale-fade" alt="" />
						</a>
						<a data-lightbox="example-set" href="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/83c2446a0896df0a1f4af01c940ae1d9_M.jpg" title="Vertical - Special Edition! (cedarsphoto)" data-label="Gray">
							<img src="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/83c2446a0896df0a1f4af01c940ae1d9_M.jpg" class="grayscale grayscale-fade" alt="" />
						</a>
						<a data-lightbox="example-set" href="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/9415f9bcd76598f9c08127db1641b596_M.jpg" title="Racing against the Protons (tom.leuzi)">
							<img src="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/9415f9bcd76598f9c08127db1641b596_M.jpg" alt="" class="grayscale grayscale-fade"/>
						</a>
						<a data-lightbox="example-set" href="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/9415f9bcd76598f9c08127db1641b596_M.jpg" title="Lupines (Kiddi Einars)">
							<img src="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/9415f9bcd76598f9c08127db1641b596_M.jpg" alt="" class="grayscale grayscale-fade"/>
						</a>
						<a data-lightbox="example-set" href="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/9415f9bcd76598f9c08127db1641b596_M.jpg" title="Lupines (Kiddi Einars)">
							<img src="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/9415f9bcd76598f9c08127db1641b596_M.jpg" alt="" class="grayscale grayscale-fade"/>
						</a>
						<a data-lightbox="example-set" href="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/83c2446a0896df0a1f4af01c940ae1d9_M.jpg" title="Morning Godafoss (Brads5)">
							<img src="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/83c2446a0896df0a1f4af01c940ae1d9_M.jpg" alt="" class="grayscale grayscale-fade"/>
						</a>
						<a data-lightbox="example-set" href="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/83c2446a0896df0a1f4af01c940ae1d9_M.jpg" title="Vertical - Special Edition! (cedarsphoto)">
							<img src="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/83c2446a0896df0a1f4af01c940ae1d9_M.jpg" alt="" class="grayscale grayscale-fade"/>
						</a>
						<a data-lightbox="example-set" href="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/9415f9bcd76598f9c08127db1641b596_M.jpg" title="Racing against the Protons (tom.leuzi)">
							<img src="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/9415f9bcd76598f9c08127db1641b596_M.jpg" alt="" class="grayscale grayscale-fade"/>
						</a>
						<a data-lightbox="example-set" href="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/9415f9bcd76598f9c08127db1641b596_M.jpg" title="Lupines (Kiddi Einars)">
							<img src="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/9415f9bcd76598f9c08127db1641b596_M.jpg" alt="" class="grayscale grayscale-fade"/>
						</a>
						<a data-lightbox="example-set" href="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/9415f9bcd76598f9c08127db1641b596_M.jpg" title="Lupines (Kiddi Einars)">
							<img src="http://1spress.ru/images/mnwallimages/400x400/media/k2/items/cache/9415f9bcd76598f9c08127db1641b596_M.jpg" alt="" class="grayscale grayscale-fade"/>
						</a>
						<div class="ajax-space" style="display: none"></div>
					</div>
					 <div class="clearfix"></div>

					<div class="text-center">
						<a class="btn gold" id="loadPhotos">больше результатов</a>
						 <div class="clearfix"></div>
						<div class="arrow"></div>
					</div>
				</div>
			 </section>

			  <div class="clearfix"></div>