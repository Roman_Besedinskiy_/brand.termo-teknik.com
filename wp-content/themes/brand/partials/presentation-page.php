		 	<section class="presentation">
			 	<div class="text-center section">
			 		<h2 class="underline">Хотите узнать больше?</h2>
			 		<div class="">
			 			<a class="btn" href="<?php echo get_field('pdf-presentation');?>" target="_blank">Скачать презентацию</a>
			 		</div>
			 		 <div class="clearfix"></div>
			 		<div class="arrow"></div>
			 	</div>
		 	</section>

		 	<div class="clearfix"></div>