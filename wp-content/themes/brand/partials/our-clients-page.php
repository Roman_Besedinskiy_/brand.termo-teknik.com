	<?php  
		$page = get_page_by_path( 'clients' );
		$clients = get_field('clients',$page->ID);   

		if ($clients):	
		?>

	    <section id="clients" class="our-clients section">
			<div class="text-center">
			 	<h2>Наши клиенты</h2>
			 	
 				<div class="container">
 				<div class="underline"></div>
 				<div class="brands">
				 <?php foreach ($clients as $cli): ?> 
				 	<div class="brands-item">
				 	     	<img class="grayscale grayscale-fade" src="<?php echo $cli['url']?>"/>
				 	</div>
				 <?php endforeach; ?>	
				 </div> 	   
				  <div class="clearfix"></div>
				  <div class="underline"></div>
				 <div class="arrow"></div>
				 </div>		 
		    </div>
	    </section> 

	    <div class="clearfix"></div>

<!-- 	<section id="clients" class="our-clients section">
		<div class="text-center">
			 	<h2>Наши клиенты</h2>
		</div>
		<div class="brands gallery">
				 <?php foreach ($clients as $cli): ?>
				  <div class="brands-item">
				 	<div class="clients-img" style="background-image: url(<?php echo $cli['url']?>);">&nbsp;</div>
				 </div>
				 <?php endforeach; ?>	
		</div>
	</section> -->



	<?php endif; ?>

 	<div class="clearfix"></div>