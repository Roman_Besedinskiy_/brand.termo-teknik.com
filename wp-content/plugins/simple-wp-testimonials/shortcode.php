<?php 
add_shortcode( 'testimonials', 'new_testimonial_shortcode' );

function new_testimonial_shortcode($atts, $content = null){ 
	extract( shortcode_atts( array(
			'title'             => '',
			'destination'             => '',
			'video'             => '',
			), $atts ) );
			
$output = '';
$argsquery = array(
	'post_type' => 'testimonial-post',
	'posts_per_page' => -1,
	'post_status' => 'publish',
	);


$output .='<div class="clearfix"></div>';
$output.='<section class="testimonials-block section"><div class="text-center">';
/*$output .='<h2> Отзывы клиентов</h2>';*/
$output .= '<div class="testimonials-bg"><div class="container">';
$output .='<h2> Отзывы клиентов</h2>';

$output .= '<div class="testimonials"><ul class="rslides text-left">';
	$the_query = new WP_Query( $argsquery );
	if( $the_query->have_posts() ) {
		$i=1;
		while ( $the_query->have_posts() ) {
	$the_query->the_post();
	$output .= '<li>';
    $output .= '<p>'.get_the_excerpt().'</p> ';
	$output .= '<h4 class="gold">'.get_post_meta( get_the_ID(), 'author_name', true ).'</h4>';
	$output .= '<div>'.get_post_meta( get_the_ID(), 'author_destination', true ).'</div>';
	$output .= '</li>';
		
		}
	
	$output .= '</ul></div>';
	$output .= '</div></div>';
	$output .= '</div></div>';
	}
	return $output;
}



    
  
   
   
