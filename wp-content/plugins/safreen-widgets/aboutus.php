<?php

/**************************/
/******service block widget */
/************************/


class safreen_aboutus extends WP_Widget {
	
	public function __construct() {
		parent::__construct(
			'ctUp-ads-aboutwidget',
			__( 'Блок - Почему наши издания лучше', 'safreen-widgets' )
		);
	}

    function widget($args, $instance) {

    echo '<section class="why"><div class="text-center section">';
    $main_title = $instance['main_title'];
    $sub_title = $instance['sub_title'];

    echo '<h2>'. $main_title .'</h2>';

    echo '<div class="container"><p>'. $sub_title .'</p><div class="underline"></div></div>';

    echo '<div class="arrow"></div></div></section>';

    // This is where you run the code and display the output

  }

    function update($new_instance, $old_instance) {

        $instance = $old_instance;
    		$instance['main_title'] = strip_tags($new_instance['main_title']);
    		$instance['sub_title'] = strip_tags($new_instance['sub_title']);
		
        return $instance;

    }
	/* ---------------------------- */
	/* ------- Display Widget -------- */
	/* ---------------------------- */


    function form($instance) {
		
		/* Set up some default widget settings. */
    if ( isset( $instance[ 'main_title' ] ) ) {
        $main_title = $instance[ 'main_title' ];
    }
    
    if ( isset( $instance[ 'sub_title' ] ) ) {
        $sub_title = $instance[ 'sub_title' ];
    }
    
    // Widget admin form
    ?>

     <p>
        <label for="<?php echo $this->get_field_id( 'main_title' ); ?>"><?php echo 'Заголовок'; ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'main_title' ); ?>" name="<?php echo $this->get_field_name( 'main_title' ); ?>" type="text" value="<?php echo esc_attr( $main_title ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'sub_title' ); ?>"><?php _e( 'О нас' ); ?></label> 
        <textarea rows="5" cols="5" class="widefat" id="<?php echo $this->get_field_id( 'sub_title' ); ?>" name="<?php echo $this->get_field_name( 'sub_title' ); ?>"><?php echo esc_attr( $sub_title ); ?></textarea>
        </p>

	<?php
    }

}



