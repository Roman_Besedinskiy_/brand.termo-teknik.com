<?php

/**************************/
/******Our Team widget */
/************************/



class safreen_statistics extends WP_Widget {
	
	public function __construct() {
		parent::__construct(
			'ctUp-ads-statisticswidget',
			__( 'Блок - Статистика заказов', 'safreen-widgets' ),
              array( 'description' => __( 'Виджет со статистикой выполненых заказов', 'safreen-widgets' ), )
		);
	}

    function widget($args, $instance) {

    echo '<section class="professionals"><div class="container block">';
    
    $title_main = $instance['title_main'];

    echo '<p class="text-center info">'. $title_main .'</p>';

    //echo '<p>'.$title_sub.'</p>';
    echo '<div class="text-center"><div class="arrow"></div></div><div class="clearfix"></div>';

    $number_1 = $instance['number_1'];
    $title_1 = $instance['title_1'];
    $text_1 = $instance['text_1'];
    // before and after widget arguments are defined by themes
    echo '<div class="col-md-4  black-border"><span class="number">'. $number_1 .'</span><div class="text-content">';

    echo '<h4>'. $title_1 .'</h4>';

    echo '<p>'. $text_1 .'</p></div></div>';

    $number_2 = $instance['number_2'];
    $title_2 = $instance['title_2'];
    $text_2 = $instance['text_2'];
    // before and after widget arguments are defined by themes
    echo '<div class="col-md-4  black-border"><span class="number">'. $number_2 .'</span><div class="text-content" style="padding-left: 11%;">';

    echo '<h4>'. $title_2 .'</h4>';

    echo '<p>'. $text_2 .'</p></div></div>';

    $number_3 = $instance['number_3'];
    $title_3 = $instance['title_3'];
    $text_3 = $instance['text_3'];
    // before and after widget arguments are defined by themes
    echo '<div class="col-md-4"><span class="number">'. $number_3 .'</span><div class="text-content">';

    echo '<h4>'. $title_3 .'</h4>';

    echo '<p>'. $text_3 .'</p></div></div>';

/*    $number_4 = $instance['number_4'];
    $title_4 = $instance['title_4'];
    $text_4 = $instance['text_4'];
    // before and after widget arguments are defined by themes
    echo '<div class="col-md-3"><span class="number">'. $number_4 .'</span><div class="text-content">';

    echo '<h4>'. $title_4 .'</h4>';

    echo '<p>'. $text_4 .'</p></div></div>';*/
    echo '</div><div class="text-center"><div class="arrow"></div></div></section>';
		

    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
            $instance = array();
            $instance['number_1'] = ( ! empty( $new_instance['number_1'] ) ) ? trim( $new_instance['number_1'] ) : '';
            $instance['title_1'] = ( ! empty( $new_instance['title_1'] ) ) ? trim( $new_instance['title_1'] ) : '';
            $instance['text_1'] = ( ! empty( $new_instance['text_1'] ) ) ? trim( $new_instance['text_1'] ) : '';

            $instance['number_2'] = ( ! empty( $new_instance['number_2'] ) ) ? trim( $new_instance['number_2'] ) : '';
            $instance['title_2'] = ( ! empty( $new_instance['title_2'] ) ) ? trim( $new_instance['title_2'] ) : '';
            $instance['text_2'] = ( ! empty( $new_instance['text_2'] ) ) ? trim( $new_instance['text_2'] ) : '';

            $instance['number_3'] = ( ! empty( $new_instance['number_3'] ) ) ? trim( $new_instance['number_3'] ) : '';
            $instance['title_3'] = ( ! empty( $new_instance['title_3'] ) ) ? trim( $new_instance['title_3'] ) : '';
            $instance['text_3'] = ( ! empty( $new_instance['text_3'] ) ) ? trim( $new_instance['text_3'] ) : '';

            $instance['number_4'] = ( ! empty( $new_instance['number_4'] ) ) ? trim( $new_instance['number_4'] ) : '';
            $instance['title_4'] = ( ! empty( $new_instance['title_4'] ) ) ? trim( $new_instance['title_4'] ) : '';
            $instance['text_4'] = ( ! empty( $new_instance['text_4'] ) ) ? trim( $new_instance['text_4'] ) : '';

            $instance['title_main'] = ( ! empty( $new_instance['title_main'] ) ) ? trim( $new_instance['title_main'] ) : '';
            $instance['title_sub'] = ( ! empty( $new_instance['title_sub'] ) ) ? trim( $new_instance['title_sub'] ) : '';

            return $instance;
        }
	/* ---------------------------- */
	/* ------- Display Widget -------- */
	/* ---------------------------- */


// Widget Backend 
public function form( $instance ) {
    if ( isset( $instance[ 'number_1' ] ) ) {
        $number_1 = $instance[ 'number_1' ];
    }
    if ( isset( $instance[ 'title_1' ] ) ) {
        $title_1 = $instance[ 'title_1' ];
    }
    if ( isset( $instance[ 'text_1' ] ) ) {
        $text_1 = $instance[ 'text_1' ];
    }

    if ( isset( $instance[ 'number_2' ] ) ) {
        $number_2 = $instance[ 'number_2' ];
    }
    if ( isset( $instance[ 'title_2' ] ) ) {
        $title_2 = $instance[ 'title_2' ];
    }
    if ( isset( $instance[ 'text_2' ] ) ) {
        $text_2 = $instance[ 'text_2' ];
    }
    

    if ( isset( $instance[ 'number_3' ] ) ) {
        $number_3 = $instance[ 'number_3' ];
    }
    if ( isset( $instance[ 'title_3' ] ) ) {
        $title_3 = $instance[ 'title_3' ];
    }
    if ( isset( $instance[ 'text_3' ] ) ) {
        $text_3 = $instance[ 'text_3' ];
    }

    if ( isset( $instance[ 'number_4' ] ) ) {
        $number_4 = $instance[ 'number_4' ];
    }
    if ( isset( $instance[ 'title_4' ] ) ) {
        $title_4 = $instance[ 'title_4' ];
    }
    if ( isset( $instance[ 'text_4' ] ) ) {
        $text_4 = $instance[ 'text_4' ];
    }
    if ( isset( $instance[ 'title_main' ] ) ) {
        $title_main = $instance[ 'title_main' ];
    }
    if ( isset( $instance[ 'title_sub' ] ) ) {
        $title_main = $instance[ 'title_sub' ];
    }
    
    // Widget admin form
    ?>

        <p>
            <label for="<?php echo $this->get_field_id('title_main'); ?>"><?php echo 'Общий заголовок'; ?></label><br/>
            <input type="text" name="<?php echo $this->get_field_name('title_main'); ?>" id="<?php echo $this->get_field_id('title_main'); ?>" value="<?php if( !empty($instance['title_main']) ): echo $instance['title_main']; endif; ?>" class="widefat">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('title_sub'); ?>"><?php echo 'Подзаголовок'; ?></label><br/>
            <textarea type="text" name="<?php echo $this->get_field_name('title_sub'); ?>" id="<?php echo $this->get_field_id('title_sub'); ?>"> <?php if( !empty($instance['title_sub']) ): echo $instance['title_sub']; endif; ?></textarea>
        </p>

    <div class="accordion_safreen">
    
    <h4 > <?php _e('Блок 1', 'safreen-widgets') ?></h4>
         
    <div class="pane_safreen">      
    <div class="widget_input_wrap">
        <p>
        <label for="<?php echo $this->get_field_id( 'number_1' ); ?>"><?php echo 'Количество'; ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'number_1' ); ?>" name="<?php echo $this->get_field_name( 'number_1' ); ?>" type="text" value="<?php echo esc_attr( $number_1 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'title_1' ); ?>"><?php echo 'Заголовок'; ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title_1' ); ?>" name="<?php echo $this->get_field_name( 'title_1' ); ?>" type="text" value="<?php echo esc_attr( $title_1 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'text_1' ); ?>"><?php echo 'Текст'; ?></label> 
         <textarea rows="5" cols="5" class="widefat" id="<?php echo $this->get_field_id( 'text_1' ); ?>" name="<?php echo $this->get_field_name( 'text_1' ); ?>"> <?php echo esc_attr( $text_1 ); ?></textarea>
        </p>
    </div>
    </div>

    <h4 > <?php _e('Блок 2', 'safreen-widgets') ?></h4>
         
    <div class="pane_safreen">      
    <div class="widget_input_wrap">
        <p>
        <label for="<?php echo $this->get_field_id( 'number_2' ); ?>"><?php echo 'Количество'; ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'number_2' ); ?>" name="<?php echo $this->get_field_name( 'number_2' ); ?>" type="text" value="<?php echo esc_attr( $number_2 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'title_2' ); ?>"><?php echo 'Заголовок'; ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title_2' ); ?>" name="<?php echo $this->get_field_name( 'title_2' ); ?>" type="text" value="<?php echo esc_attr( $title_2 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'text_2' ); ?>"<?php echo 'Текст'; ?></label> 
        <textarea rows="5" cols="5" class="widefat" id="<?php echo $this->get_field_id( 'text_2' ); ?>" name="<?php echo $this->get_field_name( 'text_2' ); ?>"> <?php echo esc_attr( $text_2 ); ?></textarea>
        </p>
    </div>
    </div>

    <h4 > <?php _e('Блок 3', 'safreen-widgets') ?></h4>
         
    <div class="pane_safreen">      
    <div class="widget_input_wrap">
        <p>
        <label for="<?php echo $this->get_field_id( 'number_3' ); ?>"><?php echo 'Количество'; ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'number_3' ); ?>" name="<?php echo $this->get_field_name( 'number_3' ); ?>" type="text" value="<?php echo esc_attr( $number_3 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'title_3' ); ?>"><?php echo 'Заголовок'; ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title_3' ); ?>" name="<?php echo $this->get_field_name( 'title_3' ); ?>" type="text" value="<?php echo esc_attr( $title_3 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'text3' ); ?>"><?php echo 'Текст'; ?></label> 
        <textarea rows="5" cols="5" class="widefat" id="<?php echo $this->get_field_id( 'text_3' ); ?>" name="<?php echo $this->get_field_name( 'text_3' ); ?>"><?php echo esc_attr( $text_3 ); ?></textarea>
        </p>
    </div>
    </div>

    <h4 > <?php _e('Блок 4', 'safreen-widgets') ?></h4>
         
    <div class="pane_safreen">      
    <div class="widget_input_wrap">
        <p>
        <label for="<?php echo $this->get_field_id( 'number_4' ); ?>"><?php echo 'Количество'; ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'number_4' ); ?>" name="<?php echo $this->get_field_name( 'number_4' ); ?>" type="text" value="<?php echo esc_attr( $number_4 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'title_4' ); ?>"><?php echo 'Заголовок'; ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title_4' ); ?>" name="<?php echo $this->get_field_name( 'title_4' ); ?>" type="text" value="<?php echo esc_attr( $title_4 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'text4' ); ?>"><?php echo 'Текст'; ?></label> 
        <textarea rows="5" cols="5" class="widefat" id="<?php echo $this->get_field_id( 'text_4' ); ?>" name="<?php echo $this->get_field_name( 'text_4' ); ?>"><?php echo esc_attr( $text_3 ); ?></textarea>
        </p>
    </div>
    </div>
    </div>
    <?php 
}

}



