<?php

/**************************/
/******Our Team widget */
/************************/



class safreen_points extends WP_Widget {
	
	public function __construct() {
		parent::__construct(
			'ctUp-ads-pointswidget',
			__( 'Блок - Пункты', 'safreen-widgets' ),
              array( 'description' => __( 'Виджет под слайдером на главной странице', 'safreen-widgets' ), )
		);
	}

    function widget($args, $instance) {

    echo '<section class="points"><div class="container">';
    $title_1 = $instance['title_1'];
    $text_1 = $instance['text_1'];
    // before and after widget arguments are defined by themes
    echo '<div class="col-md-4 col-sm-4 border-white">';

    echo '<h3>'. $title_1 .'</h3><img src="/wp-content/themes/brand/img/01.png" />';

    echo '<p>'. $text_1 .'</p></div>';

    $title_2 = $instance['title_2'];
    $text_2 = $instance['text_2'];
    // before and after widget arguments are defined by themes
    echo '<div class="col-md-4 col-sm-4 border-white">';

    echo '<h3>'. $title_2 .'</h3><img src="/wp-content/themes/brand/img/02.png" />';

    echo '<p>'. $text_2 .'</p></div>';

    $title_3 = $instance['title_3'];
    $text_3 = $instance['text_3'];
    // before and after widget arguments are defined by themes
    echo '<div class="col-md-4 col-sm-4 no-border">';

    echo '<h3>'. $title_3 .'</h3><img src="/wp-content/themes/brand/img/03.png" />';

    echo '<p>'. $text_3 .'</p></div>';
    echo '</div></section><div class="clearfix"></div><div class="text-center section"><div class="arrow"></div></div>';
		

    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
            $instance = array();
            $instance['title_1'] = ( ! empty( $new_instance['title_1'] ) ) ? trim( $new_instance['title_1'] ) : '';
            $instance['text_1'] = ( ! empty( $new_instance['text_1'] ) ) ? trim( $new_instance['text_1'] ) : '';

            $instance['title_2'] = ( ! empty( $new_instance['title_2'] ) ) ? trim( $new_instance['title_2'] ) : '';
            $instance['text_2'] = ( ! empty( $new_instance['text_2'] ) ) ? trim( $new_instance['text_2'] ) : '';

            $instance['title_3'] = ( ! empty( $new_instance['title_3'] ) ) ? trim( $new_instance['title_3'] ) : '';
            $instance['text_3'] = ( ! empty( $new_instance['text_3'] ) ) ? trim( $new_instance['text_3'] ) : '';

            return $instance;
        }
	/* ---------------------------- */
	/* ------- Display Widget -------- */
	/* ---------------------------- */


// Widget Backend 
public function form( $instance ) {
    if ( isset( $instance[ 'title_1' ] ) ) {
        $title_1 = $instance[ 'title_1' ];
    }
    if ( isset( $instance[ 'text_1' ] ) ) {
        $text_1 = $instance[ 'text_1' ];
    }

    if ( isset( $instance[ 'title_2' ] ) ) {
        $title_2 = $instance[ 'title_2' ];
    }
    if ( isset( $instance[ 'text_2' ] ) ) {
        $text_2 = $instance[ 'text_2' ];
    }
    

    if ( isset( $instance[ 'title_3' ] ) ) {
        $title_3 = $instance[ 'title_3' ];
    }
    if ( isset( $instance[ 'text_3' ] ) ) {
        $text_3 = $instance[ 'text_3' ];
    }
    
    
    // Widget admin form
    ?>
    <div class="accordion_safreen">
    
    <h4 > <?php _e('Блок 1', 'safreen-widgets') ?></h4>
         
    <div class="pane_safreen">      
    <div class="widget_input_wrap">
        <p>
        <label for="<?php echo $this->get_field_id( 'title_1' ); ?>"><?php echo 'Заголовок' ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title_1' ); ?>" name="<?php echo $this->get_field_name( 'title_1' ); ?>" type="text" value="<?php echo esc_attr( $title_1 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'text_1' ); ?>"><?php echo 'Текст' ?></label> 
        <textarea rows="5" cols="5" class="widefat" id="<?php echo $this->get_field_id( 'text_1' ); ?>" name="<?php echo $this->get_field_name( 'text_1' ); ?>"><?php echo esc_attr( $text_1 ); ?></textarea>
        </p>
    </div>
    </div>

    <h4 > <?php _e('Блок 2', 'safreen-widgets') ?></h4>
         
    <div class="pane_safreen">      
    <div class="widget_input_wrap">
        <p>
        <label for="<?php echo $this->get_field_id( 'title_2' ); ?>"><?php echo 'Заголовок' ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title_2' ); ?>" name="<?php echo $this->get_field_name( 'title_2' ); ?>" type="text" value="<?php echo esc_attr( $title_2 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'text_2' ); ?>"><?php echo 'Текст' ?></label> 
        <textarea rows="5" cols="5" class="widefat" id="<?php echo $this->get_field_id( 'text_2' ); ?>" name="<?php echo $this->get_field_name( 'text_2' ); ?>"> <?php echo esc_attr( $text_2 ); ?></textarea>
        </p>
    </div>
    </div>

    <h4 > <?php _e('Блок 3', 'safreen-widgets') ?></h4>
         
    <div class="pane_safreen">      
    <div class="widget_input_wrap">
        <p>
        <label for="<?php echo $this->get_field_id( 'title_3' ); ?>"><?php echo 'Заголовок' ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title_3' ); ?>" name="<?php echo $this->get_field_name( 'title_3' ); ?>" type="text" value="<?php echo esc_attr( $title_3 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'text3' ); ?>"><?php echo 'Текст' ?></label> 
        <textarea rows="5" cols="5" class="widefat" id="<?php echo $this->get_field_id( 'text_3' ); ?>" name="<?php echo $this->get_field_name( 'text_3' ); ?>"> <?php echo esc_attr( $text_3 ); ?></textarea>
        </p>
    </div>
    </div>
    </div>
    <?php 
}

}



