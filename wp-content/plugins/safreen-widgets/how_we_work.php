<?php

/**************************/
/******Our Team widget */
/************************/



class safreen_works extends WP_Widget {
	
	public function __construct() {
		parent::__construct(
			'ctUp-ads-workswidget',
			__( 'Блок - Как мы работаем', 'safreen-widgets' ),
              array( 'description' => __( 'Пошагавое описание работ', 'safreen-widgets' ), )
		);
	}

    function widget($args, $instance) {

    echo '<section class="how_work"><div class="section text-center">';
    echo '<h2>Как мы работаем?</h2>';
    echo '<div class="catalog"><div class="container">';
    $title_1 = $instance['title_1'];
    $text_1 = $instance['text_1'];
    // before and after widget arguments are defined by themes
    echo '<div class="case col-md-4 col-sm-6 col-xs-6 black-border"><span class="number">1</span>';

    echo '<h4 class="right-arrow">'. $title_1 .'</h4>';

    echo '<p>'. $text_1 .'</p></div>';

    $title_2 = $instance['title_2'];
    $text_2 = $instance['text_2'];
    // before and after widget arguments are defined by themes
    echo '<div class="case col-md-4 col-sm-6 col-xs-6 black-border"><span class="number">2</span>';

    echo '<h4 class="right-arrow">'. $title_2 .'</h4>';

    echo '<p>'. $text_2 .'</p></div>';

    $title_3 = $instance['title_3'];
    $text_3 = $instance['text_3'];
    // before and after widget arguments are defined by themes
    echo '<div class="case col-md-4 col-sm-6 col-xs-6 no-border"><span class="number">3</span>';

    echo '<h4 class="right-arrow">'. $title_3 .'</h4>';

    echo '<p>'. $text_3 .'</p></div>';

    $title_4 = $instance['title_4'];
    $text_4 = $instance['text_4'];
    // before and after widget arguments are defined by themes
    echo '<div class="case col-md-4 col-sm-6 col-xs-6 black-border"><span class="number">4</span>';

    echo '<h4 class="right-arrow">'. $title_4 .'</h4>';

    echo '<p>'. $text_4 .'</p></div>';

    $title_5 = $instance['title_5'];
    $text_5 = $instance['text_5'];
    // before and after widget arguments are defined by themes
    echo '<div class="case col-md-4 col-sm-6 col-xs-6 black-border"><span class="number">5</span>';

    echo '<h4 class="right-arrow">'. $title_5 .'</h4>';

    echo '<p>'. $text_5 .'</p></div>';

    $title_6 = $instance['title_6'];
    $text_6 = $instance['text_6'];
    // before and after widget arguments are defined by themes
    echo '<div class="case col-md-4 col-sm-6 col-xs-6 no-border"><span class="number">6</span>';

    echo '<h4 class="right-arrow">'. $title_6 .'</h4>';

    echo '<p>'. $text_6 .'</p></div>';

    echo '</div></div><div class ="arrow"></div>';

    echo '</div></section>';
		

    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
            $instance = array();
            $instance['title_1'] = ( ! empty( $new_instance['title_1'] ) ) ? trim( $new_instance['title_1'] ) : '';
            $instance['text_1'] = ( ! empty( $new_instance['text_1'] ) ) ? trim( $new_instance['text_1'] ) : '';

            $instance['title_2'] = ( ! empty( $new_instance['title_2'] ) ) ? trim( $new_instance['title_2'] ) : '';
            $instance['text_2'] = ( ! empty( $new_instance['text_2'] ) ) ? trim( $new_instance['text_2'] ) : '';

            $instance['title_3'] = ( ! empty( $new_instance['title_3'] ) ) ? trim( $new_instance['title_3'] ) : '';
            $instance['text_3'] = ( ! empty( $new_instance['text_3'] ) ) ? trim( $new_instance['text_3'] ) : '';

            $instance['title_4'] = ( ! empty( $new_instance['title_4'] ) ) ? trim( $new_instance['title_4'] ) : '';
            $instance['text_4'] = ( ! empty( $new_instance['text_4'] ) ) ? trim( $new_instance['text_4'] ) : '';

            $instance['title_5'] = ( ! empty( $new_instance['title_5'] ) ) ? trim( $new_instance['title_5'] ) : '';
            $instance['text_5'] = ( ! empty( $new_instance['text_5'] ) ) ? trim( $new_instance['text_5'] ) : '';

            $instance['title_6'] = ( ! empty( $new_instance['title_6'] ) ) ? trim( $new_instance['title_6'] ) : '';
            $instance['text_6'] = ( ! empty( $new_instance['text_6'] ) ) ? trim( $new_instance['text_6'] ) : '';

            return $instance;
        }
	/* ---------------------------- */
	/* ------- Display Widget -------- */
	/* ---------------------------- */


// Widget Backend 
public function form( $instance ) {
    if ( isset( $instance[ 'title_1' ] ) ) {
        $title_1 = $instance[ 'title_1' ];
    }
    if ( isset( $instance[ 'text_1' ] ) ) {
        $text_1 = $instance[ 'text_1' ];
    }

    if ( isset( $instance[ 'title_2' ] ) ) {
        $title_2 = $instance[ 'title_2' ];
    }
    if ( isset( $instance[ 'text_2' ] ) ) {
        $text_2 = $instance[ 'text_2' ];
    }
    

    if ( isset( $instance[ 'title_3' ] ) ) {
        $title_3 = $instance[ 'title_3' ];
    }
    if ( isset( $instance[ 'text_3' ] ) ) {
        $text_3 = $instance[ 'text_3' ];
    }
    
    if ( isset( $instance[ 'title_4' ] ) ) {
        $title_4 = $instance[ 'title_4' ];
    }
    if ( isset( $instance[ 'text_5' ] ) ) {
        $text_4 = $instance[ 'text_5' ];
    }

    if ( isset( $instance[ 'title_5' ] ) ) {
        $title_5 = $instance[ 'title_5' ];
    }
    if ( isset( $instance[ 'text_5' ] ) ) {
        $text_5 = $instance[ 'text_5' ];
    }

    if ( isset( $instance[ 'title_6' ] ) ) {
        $title_6 = $instance[ 'title_6' ];
    }
    if ( isset( $instance[ 'text_6' ] ) ) {
        $text_6 = $instance[ 'text_6' ];
    } 


    
    // Widget admin form
    ?>
    <div class="accordion_safreen">
    
    <h4 > <?php _e('Блок 1', 'safreen-widgets') ?></h4>
         
    <div class="pane_safreen">      
    <div class="widget_input_wrap">
        <p>
        <label for="<?php echo $this->get_field_id( 'title_1' ); ?>"><?php echo 'Заголовок' ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title_1' ); ?>" name="<?php echo $this->get_field_name( 'title_1' ); ?>" type="text" value="<?php echo esc_attr( $title_1 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'text_1' ); ?>"><?php echo 'Текст' ?></label> 
        <textarea rows="5" cols="5" class="widefat" id="<?php echo $this->get_field_id( 'text_1' ); ?>" name="<?php echo $this->get_field_name( 'text_1' ); ?>"><?php echo esc_attr( $text_1 ); ?></textarea>
        </p>
    </div>
    </div>

    <h4 > <?php _e('Блок 2', 'safreen-widgets') ?></h4>
         
    <div class="pane_safreen">      
    <div class="widget_input_wrap">
        <p>
        <label for="<?php echo $this->get_field_id( 'title_2' ); ?>"><?php echo 'Заголовок' ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title_2' ); ?>" name="<?php echo $this->get_field_name( 'title_2' ); ?>" type="text" value="<?php echo esc_attr( $title_2 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'text_2' ); ?>"><?php echo 'Текст' ?></label> 
        <textarea rows="5" cols="5" class="widefat" id="<?php echo $this->get_field_id( 'text_2' ); ?>" name="<?php echo $this->get_field_name( 'text_2' ); ?>"> <?php echo esc_attr( $text_2 ); ?></textarea>
        </p>
    </div>
    </div>

    <h4 > <?php _e('Блок 3', 'safreen-widgets') ?></h4>
         
    <div class="pane_safreen">      
    <div class="widget_input_wrap">
        <p>
        <label for="<?php echo $this->get_field_id( 'title_3' ); ?>"><?php echo 'Заголовок' ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title_3' ); ?>" name="<?php echo $this->get_field_name( 'title_3' ); ?>" type="text" value="<?php echo esc_attr( $title_3 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'text3' ); ?>"><?php echo 'Текст' ?></label> 
        <textarea rows="5" cols="5" class="widefat" id="<?php echo $this->get_field_id( 'text_3' ); ?>" name="<?php echo $this->get_field_name( 'text_3' ); ?>"> <?php echo esc_attr( $text_3 ); ?></textarea>
        </p>
    </div>
    </div>


    <h4 > <?php _e('Блок 4', 'safreen-widgets') ?></h4>
         
    <div class="pane_safreen">      
    <div class="widget_input_wrap">
        <p>
        <label for="<?php echo $this->get_field_id( 'title_4' ); ?>"><?php echo 'Заголовок' ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title_4' ); ?>" name="<?php echo $this->get_field_name( 'title_4' ); ?>" type="text" value="<?php echo esc_attr( $title_4 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'text_4' ); ?>"><?php echo 'Текст' ?></label> 
        <textarea rows="5" cols="5" class="widefat" id="<?php echo $this->get_field_id( 'text_4' ); ?>" name="<?php echo $this->get_field_name( 'text_4' ); ?>"><?php echo esc_attr( $text_1 ); ?></textarea>
        </p>
    </div>
    </div>

    <h4 > <?php _e('Блок 5', 'safreen-widgets') ?></h4>
         
    <div class="pane_safreen">      
    <div class="widget_input_wrap">
        <p>
        <label for="<?php echo $this->get_field_id( 'title_5' ); ?>"><?php echo 'Заголовок' ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title_5' ); ?>" name="<?php echo $this->get_field_name( 'title_5' ); ?>" type="text" value="<?php echo esc_attr( $title_5 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'text_5' ); ?>"><?php echo 'Текст' ?></label> 
        <textarea rows="5" cols="5" class="widefat" id="<?php echo $this->get_field_id( 'text_5' ); ?>" name="<?php echo $this->get_field_name( 'text_5' ); ?>"> <?php echo esc_attr( $text_2 ); ?></textarea>
        </p>
    </div>
    </div>

    <h4 > <?php _e('Блок 6', 'safreen-widgets') ?></h4>
         
    <div class="pane_safreen">      
    <div class="widget_input_wrap">
        <p>
        <label for="<?php echo $this->get_field_id( 'title_6' ); ?>"><?php echo 'Заголовок' ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title_6' ); ?>" name="<?php echo $this->get_field_name( 'title_6' ); ?>" type="text" value="<?php echo esc_attr( $title_6 ); ?>" />
        </p>
        <p>
        <label for="<?php echo $this->get_field_id( 'text6' ); ?>"><?php echo 'Текст' ?></label> 
        <textarea rows="5" cols="5" class="widefat" id="<?php echo $this->get_field_id( 'text_6' ); ?>" name="<?php echo $this->get_field_name( 'text_6' ); ?>"> <?php echo esc_attr( $text_3 ); ?></textarea>
        </p>
    </div>
    </div>

    </div>
    <?php 
}

}



