<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'brand');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'G{.<{YS#(DRg#S*bK:CCytc55knl>+YHWls$)$*hsE,4rqviQ5N;,Bw;4sPv*+Wl');
define('SECURE_AUTH_KEY',  'S^}vj%~B%)@9WgJMY96,)yITcoxf9;B{&W%@/r[GW-;$>GF5=rl*Z2m8p3<heE.-');
define('LOGGED_IN_KEY',    '#=`Y;A`Z)>nh`2k345 brE&#-YCz2O4;JEw,f/-} i&&zI:`@}}!gmE3<RTwBuOS');
define('NONCE_KEY',        '7`3!R6+* X.^X-4lis0ik={!0c&%+i]+a$n[[=`QXXyVjFgdV4,9}./=zsquRPKJ');
define('AUTH_SALT',        'Yf2yTc#;ll,N k]r9N0;~VJ<wjzr0r4j*Pa:*?;WI<RxBv.s=ZcZ5dm{%#_zz)XB');
define('SECURE_AUTH_SALT', '{i{.6m(w.SxmPLlU,c@f#M1}8-t3BmGfWz#y>Pg?}wHyeKcv4&Ozs]jjUhj@N72p');
define('LOGGED_IN_SALT',   'AaBhphk|,kYt*_sjb-nCnbmT] !u.: M;!;W/M>[6|-h]]tehO|OO5`enno^c99F');
define('NONCE_SALT',       '.T=iEr(ESlf#>}P7!m.NI(Nfha6d~A1aW*r y_`Ld5QK.Ax?Pig7Uo,D@vrXbO.Z');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
